# gw-hom

### Running the code
1. Run PE with your favourite approach, here we use `open_data_cpnest_script.py`, or the equivalent injection script. These scripts take an output directory and number as a random seed for the cpnest sampler as input.
  - note that this may require multiple fiducial PE runs in order to acquire enough samples such that the reweighted result is well sampled
2. Combine the multiple files via the `output_file_combiner.py` script
3. Start the overlap calculations via some cluster job submission process for `overlap_correction.py`, with arguments for the directory, start sample and end sample for the output file.
4. Combine these overlap files into one corrected `bilby` result object through `correction_combiner.py`, taking the directory within which the `corrections/` folder is contained.
5. Compute the HM likelihood and weights for the corrected set of data via `likelihood_reevaluation.py`, again taking the directory, start and end samples.
6. Penultimately, combine these files with `eval_combiner.py`, again including a directory as an argument.
7. Finally, we can plot and compute the relevant results via `plot_results.py`, again feeding the code the directory. 
