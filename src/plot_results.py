import matplotlib as mpl; mpl.use("agg")
import bilby
import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib
from matplotlib.patches import Patch

params = {
   'axes.labelsize': 18,
   'font.size': 24,
   'legend.fontsize': 20,
   'xtick.labelsize': 20,
   'ytick.labelsize': 20,
   'axes.titlesize':20,
   'text.usetex': True,
   'figure.figsize': [8, 5],
   'font.family':'sans',
   'font.serif':'DejaVu'
   }
matplotlib.rcParams.update(params)

outdir = sys.argv[1]

# Read in the results
result = bilby.core.result.read_in_result(outdir+'/corrected_result.json')
event = outdir.split('/')[-2]

if result.injection_parameters:
    injected = True
else:
    injected = False

new_likelihood_results = np.genfromtxt(outdir+'/new_likelihoods.dat')
# Get the time of the event for the calculation
try:
    data = np.genfromtxt(outdir+'/time_data.dat')
    time_of_event = data[0]; start_time = data[1]; duration = data[2]
    minimum_frequency = data[3]; sampling_frequency = data[4]
    #time_of_event = data
except:
    time_of_event = result.injection_parameters['geocent_time']
    injected = True
    result.injection_parameters = dict(bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters(
        result.injection_parameters)[0])
    print(result.injection_parameters)
    result.injection_parameters['total_mass'] = result.injection_parameters['mass_1'] + result.injection_parameters['mass_2']

overlaps = np.genfromtxt(outdir+'/overlaps.dat')
weights = new_likelihood_results[:,-1]
likelihoods_22 = new_likelihood_results[:,0]
likelihoods_HM = new_likelihood_results[:,1]
log_weights = np.log(weights)
number_of_samples = len(log_weights)
normalized_weights = weights/np.sum(weights)

result.posterior['relative_time'] = result.posterior['geocent_time'] - time_of_event

# print('maximum bad overlap = {}'.format(np.max(weights[overlaps < 0.9])))
# print('number of bad samples = {}'.format(len(weights[overlaps < 0.9])))
# print('fraction of cuts = {}'.format(float(len(weights[overlaps < 0.9]))/float(len(weights))))

custom_legend = [Patch(facecolor='C0', edgecolor='k',
                       label='IMRPhenomPv2'),
                 Patch(facecolor='C2', edgecolor='k',
                       label='NRHybSur3dq8')
                ]

# Intrinstic parameter plot
i_parameters=['total_mass', 'mass_ratio', 'chi_eff']
i_labels = [r'$M$ [M$_{\odot}$]', r'$q$', r'$\chi_\textrm{eff}$']
e_parameters=['luminosity_distance', 'iota', 'psi', 'phase']
e_labels = [r'$d_L$ [Mpc]', r'$\iota$', r'$\psi$', r'$\phi$']
# e_true = [result.injection_parameters['luminosity_distance'], result.injection_parameters['theta_jn'],
#           result.injection_parameters['psi'],result.injection_parameters['phase']]
other_parameters=['ra', 'dec', 'relative_time']
other_labels = [r'RA', r'DEC', r'$\delta t_c$']

f = result.plot_corner(save=False, label='22', color='C0',
        parameters=i_parameters, labels=i_labels, show_titles=False, quantiles=None)

if injected:

    i_true = [result.injection_parameters['total_mass'], result.injection_parameters['mass_ratio'],
              result.injection_parameters['chi_eff']]

    result.plot_corner(fig=f, save=False, weights=weights, label='HM weighted',
                       color='C2', quantiles=None, labels=i_labels,
                       parameters=i_parameters, truths=i_true, show_titles=False)
else:
    result.plot_corner(fig=f, save=False, weights=weights, label='HM weighted',
                       color='C2', quantiles=None, labels=i_labels,
                       parameters=i_parameters, show_titles=False)

#plt.tight_layout()
f.savefig(outdir+'/{}_intrinsic.pdf'.format(event))
plt.clf()

f2 = result.plot_corner(save=False, label='22', color='C0',
        parameters=e_parameters, labels=e_labels, show_titles=False, quantiles=None)

if injected:
    e_true = [result.injection_parameters['luminosity_distance'], result.injection_parameters['iota'],
              result.injection_parameters['psi']-np.pi/2,result.injection_parameters['phase']]
    result.plot_corner(fig=f2, save=False, weights=weights, label='HM weighted',
                   color='C2', quantiles=None,
                   parameters=e_parameters, labels=e_labels, truths=e_true, show_titles=False)

else:
    result.plot_corner(fig=f2, save=False, weights=weights, label='HM weighted',
                   color='C2', quantiles=None,
                   parameters=e_parameters, labels=e_labels, show_titles=False)
#plt.tight_layout()
f2.legend(handles=custom_legend, fontsize=20)
f2.savefig(outdir+'/{}_extrinsic.pdf'.format(event))
plt.clf()

f3 = result.plot_corner(save=False, label='22', color='C0',
        parameters=other_parameters, labels=other_labels)

result.plot_corner(fig=f3, save=False, weights=weights, label='HM weighted',
                   color='C2', quantiles=None,
                   parameters=other_parameters, labels=other_labels)#, truths=e_true)
#plt.tight_layout()
f3.legend(handles=custom_legend, fontsize=20)
f3.savefig(outdir+'/{}_other.pdf'.format(event))
plt.clf()

# Important values
effective_samples = np.sum(weights)**2/np.sum(weights**2)
log_weight_average = np.log(np.mean(weights))
log_reweighted_bayes_factor = log_weight_average + result.log_bayes_factor
# Importance sampling
# np.random.seed()
# boolean_sample_array = weights/np.max(weights) > np.random.rand(len(weights))
# likelihoods_HM = likelihoods_HM[boolean_sample_array]
# weights = weights[boolean_sample_array]

# Calculate all the important quantitative quantities
dimensionality_22 = np.var(likelihoods_22)*2
dimensionality_HM = (1./np.sum(weights))*np.sum(weights*likelihoods_HM**2) \
    - ((1./np.sum(weights))*np.sum(weights*likelihoods_HM))**2
dimensionality_HM *= 2
# Print the important quantities
try:
    snrs = np.sqrt(result.posterior['H1_matched_filter_snr'].real**2+ \
        result.posterior['L1_matched_filter_snr'].real**2 + \
        result.posterior['V1_matched_filter_snr'].real**2)
except:
    snrs = np.sqrt(result.posterior['H1_matched_filter_snr'].real**2+ \
        result.posterior['L1_matched_filter_snr'].real**2)


print('Maximum mf SNR   : {}'.format(np.max(snrs)))
print('================================================================')
print('Fiducial LogZ    : {}'.format(result.log_bayes_factor))
print('Corrected LogZ   : {}'.format(log_reweighted_bayes_factor))
print('LogBF HOM/2|2| : {}'.format(log_weight_average))
print('================================================================')
print('Effective Samples: {}'.format(effective_samples))
print('Total Samples    : {}'.format(number_of_samples))
print('Sample Fraction  : {}'.format(effective_samples/number_of_samples))
print('================================================================')
print('22 Dimensionality: {}'.format(dimensionality_22))
print('HM Dimensionality: {}'.format(dimensionality_HM))
print('Dimensionality D : {}'.format(dimensionality_HM-dimensionality_22))
