#!/usr/bin/env python

from __future__ import division, print_function
import matplotlib as mpl; mpl.use("agg")
import numpy as np
import bilby
import sys
import source
import source_lal
import matplotlib.pyplot as plt
import cpnest
from required_run_data import duration, sampling_frequency,  \
                              waveform_generator_22

# get the directory in which to save the result
outdir = sys.argv[1]
run_number = int(sys.argv[2])
low_or_high = outdir.split('/')[-2]

if low_or_high == "qlow":
    injection_parameters = dict(
       mass_ratio=0.8, chirp_mass=30.0, chi_1=0.4, chi_2=0.3,
       luminosity_distance=400., theta_jn=1.0, psi=2.6,
       phase=2.3, geocent_time=1126259642.413, ra=1.3, dec=-1.21)

elif low_or_high == "qhigh":
    injection_parameters = dict(
       mass_ratio=0.2, total_mass=60.0, chi_1=0.4, chi_2=0.3,
       luminosity_distance=300., theta_jn=1.0, psi=2.6,
       phase=2.3, geocent_time=1126259642.413, ra=1.3, dec=-1.21)

# Specify the name of the simulation and start logging
label = '22_pe'
bilby.core.utils.setup_logger(outdir=outdir+'/run_{}/'.format(run_number), label=label)

# specify the random seed
np.random.seed()

# lets import the data, setting up the interferometers
ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
for ifo in ifos:
    FD_strain = np.loadtxt(outdir+'/'+ifo.name+'_frequency_domain_data.dat')
    sampling_frequency = FD_strain[-1,0]*2.0
    duration = 1./(FD_strain[1,0]-FD_strain[0,0])

    ifo.set_strain_data_from_frequency_domain_strain(
        FD_strain[:,1]+1j*FD_strain[:,2], sampling_frequency=sampling_frequency,
        duration=duration,
        start_time = injection_parameters['geocent_time'] + 2 - duration
        )

# Now, we sample in chirp mass and mass ratio
priors = bilby.gw.prior.PriorDict()
priors['dec'] =  bilby.core.prior.Cosine(name='dec')
priors['ra'] =  bilby.core.prior.Uniform(name='ra', minimum=0, maximum=2 * np.pi)
priors['theta_jn'] =  bilby.core.prior.Sine(name='theta_jn')
priors['psi'] =  bilby.core.prior.Uniform(name='psi', minimum=0, maximum=np.pi)
priors['phase'] =  bilby.core.prior.Uniform(name='phase', minimum=0, maximum=2 * np.pi)

priors['chi_1'] = bilby.gw.prior.AlignedSpin(name='chi_1',
    a_prior=bilby.core.prior.Uniform(0, 0.8), latex_label=r'$\chi_1$')
priors['chi_2'] = bilby.gw.prior.AlignedSpin(name='chi_2',
    a_prior=bilby.core.prior.Uniform(0, 0.8), latex_label=r'$\chi_2$')

priors['chirp_mass'] = bilby.prior.Uniform(
    name='chirp_mass', latex_label=r'$\mathcal{M}$', minimum=5.0, maximum=70.0,
    unit='$M_{\\odot}$')

priors['mass_ratio'] = bilby.prior.Uniform(
    name='mass_ratio', latex_label='$q$', minimum=1./8., maximum=1.0)

priors['luminosity_distance'] =  bilby.gw.prior.UniformComovingVolume(name='luminosity_distance',
                                                                      minimum=1e2, maximum=5e3, unit='Mpc')

priors['geocent_time'] = bilby.core.prior.Uniform(
    minimum=injection_parameters['geocent_time'] - 0.1,
    maximum=injection_parameters['geocent_time'] + 0.1,
    name='geocent_time', latex_label='$t_c$', unit='$s$')

# Create the GW likelihood sampling in the IMRPhenomPv2 waveform
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator_22,
    priors=priors, time_marginalization=False, phase_marginalization=False)


# Run sampler, sampling all parameters
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='cpnest',
    npoints=2000,
    injection_parameters=injection_parameters, outdir=outdir+'/run_{}/'.format(run_number),
    label=label, maxmcmc=2000,
    conversion_function=bilby.gw.conversion.generate_all_bbh_parameters,
    seed=int(13*run_number), nthreads=1)

# Make a corner plot.
result.plot_corner()
