import numpy as np
import shutil
import sys
import glob
import bilby
import matplotlib.pyplot as plt

def sortKeyFunc(s):
    file = s.split('/')[-1]
    file = file.split('e')[-1]
    number = file.rstrip('.dat')
    return int(number)

outdir = sys.argv[1]

files = glob.glob(outdir+'/corrections/*.dat')
files.sort(key=sortKeyFunc)

full_array = []
i=0

ind_array = []

for file in files:
    data = np.genfromtxt(file)

    if np.ndim(data) == 1:
        data = np.array([data])
        print('dimensions are {}'.format(np.ndim(data)))

    full_array.append(data)

    ind_array.append(int(sortKeyFunc(file)/11.))

    # print for usability
    i += 1
    if i%100 == 0:
        print('At the {}th step'.format(i))

full_array = np.array(full_array)
data_array = np.concatenate(full_array,axis=0)

result_file = outdir+'/22_pe_result.json'
result = bilby.core.result.read_in_result(filename=result_file)

result_corrected = result

print(data_array[:,0])

ind_array = np.array(ind_array)
print(np.argmax(np.diff(ind_array)))
result_corrected.posterior['geocent_time'] += data_array[:,0]
result_corrected.posterior['phase'] = np.mod(data_array[:,1],2*np.pi)

result_corrected.label = 'corrected'
result_corrected.save_to_file(outdir=outdir)

overlaps = data_array[:,2]
np.savetxt(outdir+'/overlaps.dat', overlaps)

result_corrected.plot_corner(outdir=outdir)

print(np.argmax(np.isnan(overlaps)))
print(np.sum(np.isnan(overlaps)))

plt.hist(overlaps, histtype='step',bins=50)
plt.xlabel(r'$\mathcal{O}$')
plt.tight_layout()
plt.savefig(outdir+'/overlap_histogram.png')
plt.clf()
