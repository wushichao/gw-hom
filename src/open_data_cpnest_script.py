#!/usr/bin/env python

from __future__ import division, print_function
import matplotlib as mpl; mpl.use("agg")
import numpy as np
import bilby
import sys
import source
import source_lal
import matplotlib.pyplot as plt
#from required_run_data import duration, sampling_frequency, minimum_frequency

# get the directory in which to save the result
outdir = sys.argv[1]
run_number = int(sys.argv[2])

event = outdir.split('/')[-2]
print(event)

# Specify the name of the simulation and start logging
label = '22_pe'
bilby.core.utils.setup_logger(outdir=outdir+'/run_{}/'.format(run_number), label=label)

# Get the time of the event for the calculation
data = np.genfromtxt(outdir+'/time_data.dat')
time_of_event = data[0]; start_time = data[1]; duration = data[2]
minimum_frequency = data[3]; sampling_frequency = data[4]

waveform_arguments_22 = dict(waveform_approximant='IMRPhenomPv2',
                          reference_frequency=50., minimum_frequency=minimum_frequency)

waveform_generator_22 = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    waveform_arguments=waveform_arguments_22,
    start_time=start_time)

# specify the random seed
np.random.seed()

# lets import the data, setting up the interferometers
ASD_data_file = np.genfromtxt(outdir+'/pr_psd.dat')
if len(ASD_data_file[0]) == 3:
    ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
elif len(ASD_data_file[0]) == 4:
    ifos = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])

for ifo in ifos:
    FD_strain = np.loadtxt(outdir+'/'+ifo.name+'_frequency_domain_data.dat')

    ifo.minimum_frequency = minimum_frequency
    ifo.maximum_frequency = sampling_frequency/2.

    ifo.set_strain_data_from_frequency_domain_strain(
        FD_strain[:,1]+1j*FD_strain[:,2], sampling_frequency=sampling_frequency,
        duration=duration, start_time = start_time
    )

    ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity.from_amplitude_spectral_density_file(
        outdir+'/'+ifo.name+'_psd.dat'
    )
    ifo.power_spectral_density.psd_array = np.minimum(ifo.power_spectral_density.psd_array, 1)
    #ASD_data = np.genfromtxt(outdir+'/'+ifo.name+'_psd.dat')

# Now, we sample in chirp mass and mass ratio
priors = bilby.gw.prior.PriorDict()
priors['dec'] =  bilby.core.prior.Cosine(name='dec')
priors['ra'] =  bilby.core.prior.Uniform(name='ra', minimum=0, maximum=2 * np.pi)
priors['theta_jn'] =  bilby.core.prior.Sine(name='theta_jn')
priors['psi'] =  bilby.core.prior.Uniform(name='psi', minimum=0, maximum=np.pi)
priors['phase'] =  bilby.core.prior.Uniform(name='phase', minimum=0, maximum=2 * np.pi)

priors['chi_1'] = bilby.gw.prior.AlignedSpin(name='chi_1',
    a_prior=bilby.core.prior.Uniform(0, 0.8), latex_label=r'$\chi_1$')
priors['chi_2'] = bilby.gw.prior.AlignedSpin(name='chi_2',
    a_prior=bilby.core.prior.Uniform(0, 0.8), latex_label=r'$\chi_2$')

priors['chirp_mass'] = bilby.prior.Uniform(
    name='chirp_mass', latex_label='$M$', minimum=5.0, maximum=70.0,
    unit='$M_{\\odot}$')

priors['mass_ratio'] = bilby.prior.Uniform(
    name='mass_ratio', latex_label='$q$', minimum=1./8., maximum=1.0)

priors['geocent_time'] = bilby.core.prior.Uniform(
    minimum=time_of_event - 0.1,
    maximum=time_of_event + 0.1,
    name='geocent_time', latex_label='$t_c$', unit='$s$')
priors['luminosity_distance'] =  bilby.gw.prior.UniformComovingVolume(name='luminosity_distance',
                                                                      minimum=1e2, maximum=5e3, unit='Mpc')

# Create the GW likelihood sampling in the IMRPhenomPv2 waveform
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator_22,
    priors=priors, time_marginalization=False, phase_marginalization=False)


for phi in np.linspace(0,2*np.pi,10):
    params = dict(
       chirp_mass=8.56, mass_ratio=0.83, chi_1=0.07, chi_2=0.07,
       luminosity_distance=319., theta_jn=2.51, psi=2.659,
       phase=phi, geocent_time=time_of_event, ra=2.11, dec=0.42)
    likelihood.parameters = params
    print('The evaluated logL is: {}'.format(likelihood.log_likelihood_ratio()))


# Run sampler, sampling all parameters
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='cpnest',
    npoints=2000,
    outdir=outdir+'/run_{}/'.format(run_number),
    label=label, maxmcmc=2000,
    conversion_function=bilby.gw.conversion.generate_all_bbh_parameters,
    seed=int(13*run_number))

result.plot_corner()
