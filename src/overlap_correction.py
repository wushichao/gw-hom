import bilby
import numpy as np
import sys
import glob
import matplotlib.pyplot as plt
import source
import source_gws2
import os

outdir = sys.argv[1]
start_sample = int(sys.argv[2])
end_sample = int(sys.argv[3])

try:
    data = np.genfromtxt(outdir+'/time_data.dat')
    time_of_event = data[0]; start_time = data[1]; duration = data[2]
    minimum_frequency = data[3]; sampling_frequency = data[4]

except:
    #from required_run_data import duration, sampling_frequency
    print('tooooo bad')


try:
    # Create target Directory
    os.mkdir(outdir+'/corrections/')
    print("Correction Directory Created ")
except:
    print("Correction Directory already exists")

# load in result file:
result_file = outdir+'/22_pe_result.json'
result = bilby.core.result.read_in_result(filename=result_file)

result_corrected = result
posterior_dict = result.posterior
number_of_samples = len(result.posterior)

print('total number of evaluations is {}'.format(number_of_samples))

if end_sample >= number_of_samples: print('setting end sample to max sample'); end_sample = number_of_samples

if start_sample >= number_of_samples:
    raise ValueError('You are outside of the number of samples')

time_shift_array = []
phase_array = []
overlap_array = []

for i in range(start_sample,end_sample):

    injection_parameters = dict(
        mass_1=posterior_dict['mass_1'][i],
        mass_2=posterior_dict['mass_2'][i],
        chi_1=posterior_dict['chi_1'][i], chi_2=posterior_dict['chi_2'][i],
        luminosity_distance=posterior_dict['luminosity_distance'][i],
        theta_jn=posterior_dict['theta_jn'][i], psi=posterior_dict['psi'][i],
        phase=posterior_dict['phase'][i],
        geocent_time=posterior_dict['geocent_time'][i],
        ra=posterior_dict['ra'][i], dec=posterior_dict['dec'][i])

    # Then, need to call the source NRSur7dq2_overlap function
    # data = source.NRSur7dq2_overlap(frequency=np.linspace(0,sampling_frequency/2.0,int(duration*sampling_frequency/2.0 + 1)),
    #     return_correction=True,
    #     **injection_parameters)
    data = source_gws2.gws_overlap(frequency=np.linspace(0,sampling_frequency/2.0,int(duration*sampling_frequency/2.0 + 1)),
        return_correction=True,
        **injection_parameters)

    print data['t_shift'], data['phase_new'], data['overlap']
    time_shift_array.append(data['t_shift'])
    phase_array.append(data['phase_new'])
    overlap_array.append(data['overlap'])

    if np.isnan(data['overlap']):
        raise ValueError('NaN in overlap killing job')

arr = np.array([time_shift_array, phase_array, overlap_array]).T
print(arr)

np.savetxt(outdir+'/corrections/s{}e{}.dat'.format(start_sample,end_sample), arr)


# result_corrected.label = 'corrected'
# result_corrected.save_to_file(outdir=outdir)
