import matplotlib as mpl; mpl.use("agg")
import bilby
import numpy as np
import sys
import glob
import matplotlib.pyplot as plt
import source as src
import source_gws2
import os
#from required_run_data import sampling_frequency, minimum_frequency, duration # TODO REMOVE LATER

outdir = sys.argv[1]
start_sample = int(sys.argv[2])
end_sample = int(sys.argv[3])


try:
    # Create target Directory
    os.mkdir(outdir+'/HM_evaluations/')
    print("Sample Directory Created ")
except:
    print("Sample Directory already exists")

# load in result file:
result_file = outdir+'/corrected_result.json' # changed from corrected_result.json
result = bilby.core.result.read_in_result(filename=result_file)
# Get the time of the event for the calculation
data = np.genfromtxt(outdir+'/time_data.dat')
time_of_event = data[0]; start_time = data[1]; duration = data[2]
minimum_frequency = data[3]; sampling_frequency = data[4]
#time_of_event = data
#start_time = time_of_event - 2 # TODO REMOVE THESE LATER

# lets import the data, setting up the interferometers
try:
    ASD_data_file = np.genfromtxt(outdir+'/pr_psd.dat')
    if len(ASD_data_file[0]) == 3:
        ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
    elif len(ASD_data_file[0]) == 4:
        ifos = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
except:
    ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])

for ifo in ifos:
    FD_strain = np.loadtxt(outdir+'/'+ifo.name+'_frequency_domain_data.dat')

    ifo.minimum_frequency = minimum_frequency
    ifo.maximum_frequency = sampling_frequency/2.

    ifo.set_strain_data_from_frequency_domain_strain(
        FD_strain[:,1]+1j*FD_strain[:,2], sampling_frequency=sampling_frequency,
        duration=duration, start_time = start_time
    )

    ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity.from_amplitude_spectral_density_file(
        outdir+'/'+ifo.name+'_psd.dat'
    )
    ifo.power_spectral_density.psd_array = np.minimum(ifo.power_spectral_density.psd_array, 1)
    #ASD_data = np.genfromtxt(outdir+'/'+ifo.name+'_psd.dat')

# Construct the appropriate waveform generator
waveform_arguments_HM = dict(
                             reference_frequency=50., minimum_frequency=20.
                             )

waveform_generator_HM = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    #frequency_domain_source_model=src.NRSur7dq2_nominal,
    frequency_domain_source_model=source_gws2.gws_nominal,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    waveform_arguments=waveform_arguments_HM,
    start_time=start_time)

# Create the GW likelihood
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator_HM)

# Now, it is time to determine the new likelihood values
likelihoods_22 = result.posterior['log_likelihood']
print(result)
posterior_dict_22 = result.posterior
number_of_samples = len(likelihoods_22)

print(start_sample, number_of_samples)

likelihoods_HM = []
weights = []

if end_sample >= number_of_samples: print('setting end sample to max sample'); end_sample = number_of_samples

if start_sample >= number_of_samples:
    raise ValueError('You are outside of the number of samples')

for i in range(start_sample,end_sample):

    likelihood_parameters = dict(
        mass_1=posterior_dict_22['mass_1'][i],
        mass_2=posterior_dict_22['mass_2'][i],
        chi_1=posterior_dict_22['chi_1'][i], chi_2=posterior_dict_22['chi_2'][i],
        luminosity_distance=posterior_dict_22['luminosity_distance'][i],
        theta_jn=posterior_dict_22['theta_jn'][i], psi=posterior_dict_22['psi'][i],
        phase=posterior_dict_22['phase'][i],
        geocent_time=posterior_dict_22['geocent_time'][i],
        ra=posterior_dict_22['ra'][i], dec=posterior_dict_22['dec'][i])

    likelihood.parameters = likelihood_parameters
    likelihood_HM = likelihood.log_likelihood_ratio()
    weight = np.exp(likelihood_HM - likelihoods_22[i])

    likelihoods_HM.append(likelihood_HM)
    weights.append(weight)

    print(likelihoods_22[i], likelihood_HM, likelihood_HM - likelihoods_22[i])
    print('evalution {}/{}'.format(i, number_of_samples))

array_to_be_saved = np.array([likelihoods_22[start_sample:end_sample],
                              likelihoods_HM, weights]).T

np.savetxt(outdir+'/HM_evaluations/s{}e{}.dat'.format(start_sample,end_sample),
    array_to_be_saved)
