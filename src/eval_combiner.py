import numpy as np
import shutil
import sys
import glob

def sortKeyFunc(s):
    file = s.split('/')[-1]
    file = file.split('e')[-1]
    number = file.rstrip('.dat')
    return int(number)

outdir = sys.argv[1]

files = glob.glob(outdir+'/HM_evaluations/*.dat')
files.sort(key=sortKeyFunc)

full_array = []
i=0

for file in files:
    data = np.genfromtxt(file)
    if np.ndim(data) != 1:
        for entry in data:
            full_array.append(entry)
    else:
        full_array.append(data)

    # print for usability
    i += 1
    if i%100 == 0:
        print('At the {}th step'.format(i))

    
full_array = np.array(full_array)
data_array = full_array

np.savetxt(outdir+'/new_likelihoods.dat', data_array)
print('finished')

# Clean up the directory written to
#shutil.rmtree(outdir+'/HM_evaluations', ignore_errors=True)
#print('deleted the halfway files')
