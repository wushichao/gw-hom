import numpy as np
import shutil
import sys
import glob
import bilby
import pandas
import scipy.misc

outdir = sys.argv[1]
q = 1./8.
files = glob.glob(outdir+'/run_*/22_pe_result.json')

print('Number of result files: {}'.format(len(files)))

result_out = bilby.core.result.read_in_result(filename=files[0])
result_out.posterior = result_out.posterior[result_out.posterior['mass_ratio'] > q]

evidences = [result_out.log_bayes_factor]

for i, file in enumerate(files[1:]):
    print('output file: {} @ step {}'.format(file,i))

    result_in = bilby.core.result.read_in_result(filename=file)

    result_out.posterior = pandas.concat([result_out.posterior,
        result_in.posterior[result_in.posterior['mass_ratio'] > q]])

    evidences.append(result_in.log_bayes_factor)

av_log_evidence = scipy.misc.logsumexp(evidences) - np.log(len(evidences))
result_out.log_bayes_factor = av_log_evidence
print(result_out.log_bayes_factor)

print('Unique samples: {}'.format(len(result_out.posterior['mass_ratio'])))
result_out.save_to_file(outdir=outdir)
result_out.plot_corner(outdir=outdir)
